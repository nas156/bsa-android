package me.mladenrakonjac.modernandroidapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_post_details.*

class PostDetails : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post_details)

        val user = intent.getStringExtra(NAME)
        val site = intent.getStringExtra(WEBSITE)
        val mail = intent.getStringExtra(EMAIL)
        val desc = intent.getStringExtra(DESC)
        val txt = intent.getStringExtra(TEXT)

        description.text = desc
        username.text = user
        website.text = site
        email.text = mail
        text.text = txt
    }
}