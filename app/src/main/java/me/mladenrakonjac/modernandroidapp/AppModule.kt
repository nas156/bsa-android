package me.mladenrakonjac.modernandroidapp

import android.content.Context
import dagger.Module
import dagger.Provides

@Module
class AppModule {

    @Provides
    fun providesContext(application: PostApplication): Context {
        return application.applicationContext
    }
}