package me.mladenrakonjac.modernandroidapp.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.databinding.ObservableField
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import me.mladenrakonjac.modernandroidapp.data.PostRepository
import me.mladenrakonjac.modernandroidapp.data.Post
import plusAssign
import javax.inject.Inject

class MainViewModel @Inject constructor(private var postRepository: PostRepository) : ViewModel() {

    val isLoading = ObservableField(false)

    var repositories = MutableLiveData<ArrayList<Post>>()

    private val compositeDisposable = CompositeDisposable()

    fun loadRepositories() {
        isLoading.set(true)
        compositeDisposable += postRepository
                .getPosts()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableObserver<ArrayList<Post>>() {

            override fun onError(e: Throwable) {
                println(e.message)
            }

            override fun onNext(data: ArrayList<Post>) {
                repositories.value = data
            }

            override fun onComplete() {
                isLoading.set(false)
            }
        })
    }

    fun loadSavedData(){
        isLoading.set(true)
        compositeDisposable += postRepository
                .getLocalPosts()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableObserver<ArrayList<Post>>() {

                    override fun onError(e: Throwable) {
                        println(e.message)
                    }

                    override fun onNext(data: ArrayList<Post>) {
                        repositories.value = data
                    }

                    override fun onComplete() {
                        isLoading.set(false)
                    }
                })
    }

    override fun onCleared() {
        super.onCleared()
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }
}