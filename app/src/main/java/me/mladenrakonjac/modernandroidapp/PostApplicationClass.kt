package me.mladenrakonjac.modernandroidapp

import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class PostApplication : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder().create(this)
    }

}