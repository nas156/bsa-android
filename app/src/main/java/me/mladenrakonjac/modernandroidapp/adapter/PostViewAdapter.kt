package me.mladenrakonjac.modernandroidapp.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import me.mladenrakonjac.modernandroidapp.databinding.ItemPostBinding
import me.mladenrakonjac.modernandroidapp.MainActivity
import me.mladenrakonjac.modernandroidapp.data.Post


class PostViewAdapter(private var items: ArrayList<Post>,
                      private var listener: MainActivity)
    : RecyclerView.Adapter<PostViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemPostBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int)
            = holder.bind(items[position], listener)

    override fun getItemCount(): Int = items.size

    interface OnItemClickListener {
        fun onItemClick(post: Post)
    }

    fun replaceData(arrayList: ArrayList<Post>) {
        items = arrayList
        notifyDataSetChanged()
    }

    class ViewHolder(private var binding: ItemPostBinding) :
            RecyclerView.ViewHolder(binding.root) {

        fun bind(post: Post, listener: OnItemClickListener?) {
            binding.post = post
            if (listener != null) {
                binding.root.setOnClickListener { _ -> listener.onItemClick(post) }
            }

            binding.executePendingBindings()
        }
    }

}

