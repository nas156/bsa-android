package me.mladenrakonjac.modernandroidapp

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import dagger.android.support.DaggerAppCompatActivity
import me.mladenrakonjac.modernandroidapp.databinding.ActivityMainBinding
import me.mladenrakonjac.modernandroidapp.adapter.PostViewAdapter
import me.mladenrakonjac.modernandroidapp.data.Post
import me.mladenrakonjac.modernandroidapp.viewmodel.MainViewModel
import javax.inject.Inject

const val NAME = "NAME"
const val DESC = "DESC"
const val EMAIL = "EMAIL"
const val WEBSITE = "WEBSITE"
const val TEXT = "TEXT";

abstract class MainActivity : DaggerAppCompatActivity(), PostViewAdapter.OnItemClickListener {

    private lateinit var binding: ActivityMainBinding
    private val repositoryRecyclerViewAdapter = PostViewAdapter(arrayListOf(), this)

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)


        val viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(MainViewModel::class.java)
        binding.viewModel = viewModel
        binding.executePendingBindings()

        binding.repositoryRv.layoutManager = LinearLayoutManager(this)
        binding.repositoryRv.adapter = repositoryRecyclerViewAdapter
        viewModel.repositories.observe(this,
                Observer<ArrayList<Post>> { it?.let { repositoryRecyclerViewAdapter.replaceData(it) } })

    }

    override fun onItemClick(post: Post) {
        val details = Intent(this, PostDetails::class.java).apply {
            putExtra(NAME, post.user.name)
            putExtra(DESC, post.description)
            putExtra(TEXT, post.text)
            putExtra(EMAIL, post.user.email)
            putExtra(WEBSITE, post.user.website)
        }
        startActivity(details)
    }

}
