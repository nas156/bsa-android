package me.mladenrakonjac.modernandroidapp.data

class User(val name: String, val email: String, val website: String) {

    fun getWriter(): String{
        return "by $name"
    }

}