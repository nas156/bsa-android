package me.mladenrakonjac.modernandroidapp.data

import android.databinding.BaseObservable

data class Post(var text: String, var user: User, var id: Int
           , var description: String) : BaseObservable() {
}