package me.mladenrakonjac.modernandroidapp.data

import io.reactivex.Observable

class PostRemoteDataSource {

    var list = ArrayList<Post>()

    fun getRepositories(): Observable<ArrayList<Post>> {
//        val url = "http://jsonplaceholder.typicode.com/posts"
//        val request = Request.Builder().url(url).build()
//        val client = OkHttpClient()
//        client.newCall(request).enqueue(object : Callback {
//            override fun onFailure(call: Call, e: IOException) {
//                println(e.message)
//            }
//
//            override fun onResponse(call: Call, response: Response) {
//                val res = response.body?.string()
//                val json = GsonBuilder().create()
//                val user1 = User("john", "aa@aa.com", "Baker street");
//                var arrayList = ArrayList<Post>()
//                arrayList.add(Post("First From Local", user1, 100, "false"))
//                arrayList.add(Post("Second From Local", user1, 30, "true"))
//                arrayList.add(Post("Third From Local", user1, 430, "false"))
//
//                }
//            })
        val user1 = User("john", "aa@aa.com", "Baker street");
        var arrayList = ArrayList<Post>()
        arrayList.add(Post("First From Local", user1, 100, "false"))
        arrayList.add(Post("Second From Local", user1, 30, "true"))
        arrayList.add(Post("Third From Local", user1, 430, "false"))



        return Observable.just(arrayList)
    }
}



