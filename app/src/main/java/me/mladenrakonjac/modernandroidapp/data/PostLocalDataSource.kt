package me.mladenrakonjac.modernandroidapp.data

import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import java.util.concurrent.TimeUnit

class PostLocalDataSource {

    fun getRepositories(): Observable<ArrayList<Post>> {
        val user1 = User("john", "aa@aa.com", "Baker street");
        var arrayList = ArrayList<Post>()
        arrayList.add(Post("First From Local", user1, 100, "false"))
        arrayList.add(Post("Second From Local", user1, 30, "true"))
        arrayList.add(Post("Third From Local", user1, 430, "false"))

        return Observable.just(arrayList).delay(2, TimeUnit.SECONDS)
    }

    fun saveRepositories(arrayList: ArrayList<Post>): Completable {
        return Single.just(1).delay(1, TimeUnit.SECONDS).toCompletable()
    }
}
