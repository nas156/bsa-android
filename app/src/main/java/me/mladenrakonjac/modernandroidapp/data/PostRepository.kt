package me.mladenrakonjac.modernandroidapp.data

import io.reactivex.Observable
import me.mladenrakonjac.modernandroidapp.utility.NetManager
import javax.inject.Inject

class PostRepository @Inject constructor(var netManager: NetManager) {

    private val localDataSource = PostLocalDataSource()
    private val remoteDataSource = PostRemoteDataSource()

    fun getPosts(): Observable<ArrayList<Post>> {
        netManager.isConnectedToInternet?.let {
            if (it) {
                return remoteDataSource.getRepositories().flatMap {
                    return@flatMap localDataSource.saveRepositories(it)
                            .toSingleDefault(it)
                            .toObservable()
                }
            }
        }

        return localDataSource.getRepositories()
    }

    fun getLocalPosts(): Observable<ArrayList<Post>>{
        return localDataSource.getRepositories()
    }
}



